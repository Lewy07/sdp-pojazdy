import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.*;


enum Type {
    CAR,
    SHIP,
    PLANE,
    BICYCLE
}


public class Vehicles implements Comparable<Vehicles>  {

    private static final Logger logger = LogManager.getLogger(Vehicles.class);
    private final float v_max;
    private final String maker;
    private final Type type;


    public Vehicles(Type type, String maker, float v_max)
    {
        this.type = type;
        this.maker = maker;
        this.v_max = v_max;
    }

    public float getVmax()
    {
        return this.v_max;
    }

    public Type getType()
    {
        return this.type;
    }

    public String getMaker()
    {
        return this.maker;
    }

    public int compareTo(Vehicles o)
    {
        return Float.compare(o.getVmax(), this.v_max);
    }

    public static void main (String [] args)
    {
        Scanner data = new Scanner(System.in);
        boolean loop = true;
        int option;

        List<Vehicles> list = new ArrayList<>();
        list.add(new Vehicles(Type.CAR, "BMW", 250));
        list.add(new Vehicles(Type.BICYCLE, "KROSS", 70));
        list.add(new Vehicles(Type.BICYCLE, "KONA", 80));
        list.add(new Vehicles(Type.CAR, "AUDI", 306));
        list.add(new Vehicles(Type.PLANE, "BOEING", 600));
        list.add(new Vehicles(Type.PLANE, "AIRBUS", 700));
        list.add(new Vehicles(Type.SHIP, "DSME", 40));
        list.add(new Vehicles(Type.SHIP, "STX", 70));

        Collections.sort(list);

        while(loop){
            try {
                logger.info("Select an option number:\n1. CAR\n2. BIKE\n3. SHIP\n4. PLANE\n5. ALL\n9. EXIT");
                option = data.nextInt();
                switch (option) {
                    case 1:
                        showTheFastest(Type.CAR, list);
                        break;
                    case 2:
                        showTheFastest(Type.BICYCLE, list);
                        break;
                    case 3:
                        showTheFastest(Type.SHIP, list);
                        break;
                    case 4:
                        showTheFastest(Type.PLANE, list);
                        break;
                    case 5:
                        for (Vehicles x : list)
                        {
                            logger.info("Vehicle type "+ x.getType()+ " from the manufacturer "+ x.getMaker()+
                                    " has maximum speed of " + x.getVmax());
                        }
                        break;
                    case 9:
                        logger.info("EXIT");
                        loop = false;
                        break;
                    default:
                        logger.warn("Wrong option!");
                }
            } catch (java.util.InputMismatchException e) {
                logger.error("Wrong input data type!");
                data.next();
            }
        }
    }

    public static void showTheFastest(Type type, List<Vehicles> list)
    {
        for (Vehicles x : list)
        {
            if(x.getType().equals(type))
            {
                logger.info("Vehicle type "+ x.getType()+ " from the manufacturer "+ x.getMaker()+
                        " is the fastest with maximum speed " + x.getVmax());
                break;
            }
        }
    }
}
